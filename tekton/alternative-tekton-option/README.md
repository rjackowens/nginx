# Docker Build and Push Task 
Created per official [Tekton GitHub tutorial](https://github.com/tektoncd/pipeline/blob/master/docs/tutorial.md) on 2/22/21

This project uses pipeline resource files instead of explicit clone tasks and persistent volumes. For simplicity only inclues task, can be extended into pipeline with minimal effort.

## Usage

Run `gci *.yml | % {c:\tools\oc.exe apply -f $_}`